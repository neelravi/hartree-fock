

function overlap(alpha, beta)
! this subroutine calculates the overlap matrix for two center systems
use general_data
implicit double precision (a-h, o-z)
overlap = (pi/(alpha+beta))**1.5*dexp(-(alpha*beta)*(rAB2)/(alpha+beta))
!print*, "overlap from function", overlap
end function



! subroutine overlap(S)
! ! this subroutine calculates the overlap matrix for two center systems
! use general_data
! use basis_data
! implicit double precision (a-h, o-z)
! real(kind=DP), intent(out) :: S(2,2)
! character(len=60) :: name
! integer:: p,q,i

! name = "Overlap"

! call cpu_time(start)

! !scalling of exponents and coefficients
! do i = 1,3
!   expon(i,1) = expon(i,1)*zeta_H**2
!   expon(i,2) = expon(i,2)*zeta_He**2
!   coeff(i,1) = coeff(i,1)*(2.0*expon(i,1)/pi)**0.75d0
!   coeff(i,2) = coeff(i,2)*(2.0*expon(i,2)/pi)**0.75d0
! enddo

! S(1,1) = 0.d0 ; S(1,2) = 0.d0 ; S(2,1) =0.d0; S(2,2) = 0.d0! assume that this is Smunu = S(1,1) mu =1, nu =1 

! !       For H-H

! rAB2 = dabs(nuc_coord(1)-nuc_coord(2))**2
! do p = 1, 3
! do q = 1, 3
! !print*, "junk ", (pi/(expon(p,1)+expon(q,2)))**1.5
! bigexp = -(expon(p,1)*expon(q,2))*(rAB2)/(expon(p,1)+expon(q,2))        
!   S(1,1) = S(1,1) + coeff(p,1)*coeff(q,1)*(pi/(expon(p,1)+expon(q,1)))**1.5
!   S(1,2) = S(1,2) + coeff(p,1)*coeff(q,2)*(pi/(expon(p,1)+expon(q,2)))**1.5*dexp(bigexp)
!   S(2,2) = S(2,2) + coeff(p,2)*coeff(q,2)*(pi/(expon(p,2)+expon(q,2)))**1.5
! enddo
! enddo

! S(2,1) =  S(1,2)

! call printing(name,S)

! call cpu_time(finish)

! call timer(start, finish, name)

! end subroutine


