! subroutine timer(start, finish, routine)
! double precision, intent(in) ::start,finish
! character(len=60) :: routine
! write(*,*) "Calculation of ", trim(routine), " takes ", finish-start," seconds."
! end subroutine