subroutine printing(message, A)
character(len=*) :: message
double precision :: A(2,2)
integer :: i, j

print*, " "
print*, "Priting the ", trim(message),  " matrix ..."
print*, " "
do i = 1, 2
    write(*,'(a,2x,f9.6,4x,f9.6,2x,a)') "|", (A(i,j),j=1,2), "|"
enddo
print*, " "
end subroutine