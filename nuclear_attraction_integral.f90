double precision function nuclear_attraction_integral(alpha,beta,squared_rab,squared_rcp,Z)
use general_data, only : pi
double precision :: alpha, beta, F0
double precision, intent(in) :: squared_rab, squared_rcp
double precision :: Z
nuclear_attraction_integral = -((2.d0*pi*Z)/(alpha+beta))*F0((alpha+beta)*squared_rcp)*dexp(-(alpha*beta)*(squared_rab)/(alpha+beta))
end function