program hartree_fock
! Program Written by Ravindra under GPU Licence
use general_data
use basis_data

implicit double precision (a-h,o-z)

! Arrays for storing various energy terms including overlap
double precision :: overlap(nshell,nshell),        &
                    kinetic(nshell,nshell),        &
                    potentialA(nshell,nshell),     &
                    potentialB(nshell,nshell),     &
                    fock(nshell,nshell),           &
                    density(nshell,nshell),        &
                    G(nshell,nshell),              &
                    mulliken(nshell,nshell)

double precision :: hamiltonian(nshell,nshell),    &
                    unitary(nshell,nshell),        &
                    unitary_dagger(nshell,nshell), &
                    fock_dagger(nshell,nshell)

double precision :: eigenvectors(nshell,nshell),   & 
                    eigenvalues(nshell,nshell),    & 
                    C(nshell, nshell),             &
                    old_density(nshell,nshell)

double precision :: two_electron(nshell, nshell, nshell, nshell)


! local variables
integer       :: i, p , q, iter
!real(kind=DP) :: overlap_integral, &
!                 kinetic_integral, &
!                 nuclear_attraction_integral 

real(kind=DP) :: rab, rap, rbp, rbq, rpq, &
                 squared_rab, squared_rap, squared_rbp, squared_rbq, squared_rpq

!real(kind=DP) :: square1 = 0.d0 , square2 = 0.d0, square3 = 0.d0, square4 = 0.d0

! variables needed by lapack or blas routines
integer       :: info, lwork ! needed for lapack
integer, parameter :: lwmax = 1000
integer(kind=4)::lda,ldb,lde

real(kind=DP) :: work(1)  ! needed for lapack
real(kind=DP) :: alpha,beta

! energy terms
real(kind=DP) :: total_energy = 0.d0,      &
                 electronic_energy = 0.d0, &
                 delta = 0.d0

! timings and other useful information
real :: starttime, endtime
!

! interface block begins here
interface
double precision function overlap_integral(alpha,beta,squared_rab)
use general_data, only : pi
double precision :: alpha, beta, squared_rab
end function


double precision function kinetic_integral(alpha,beta,squared_rab)
use general_data, only : pi
double precision :: alpha, beta, squared_rab
end function

double precision function nuclear_attraction_integral(alpha,beta,squared_rab,squared_rcp,Z)
use general_data, only : pi
double precision :: alpha, beta, F0
double precision, intent(in) :: squared_rab, squared_rcp
double precision :: Z
end function

double precision function F0(argument)
use general_data, only : pi
double precision :: argument
end function

! this is two electron integral part
double precision function two_electron_integral(alpha, beta, gamma1, delta, squared_rab, squared_rcd, squared_rpq)
use general_data, only : pi
double precision :: alpha, beta, gamma1, delta, F0
double precision, intent(in) :: squared_rab, squared_rcd, squared_rpq
end function

end interface

! interface block ends here

call cpu_time(starttime)



! The scaling of coefficients and exponents 
!scaled_expon(:,1) = expon(:,N)*zeta_He**2
!scaled_coeff(:,1) = coeff(:,N)*(2.0*scaled_expon(:,1)/pi)**0.75d0
!scaled_expon(:,2) = expon(:,N)*zeta_H**2
!scaled_coeff(:,2) = coeff(:,N)*(2.0*scaled_expon(:,2)/pi)**0.75d0

scaled_coeff(:,1) = scaled_coeff(:,1)   *  (2.0*scaled_expon(:,1)/pi)**0.75d0
scaled_coeff(:,2) = scaled_coeff(:,2)   *  (2.0*scaled_expon(:,2)/pi)**0.75d0

! initialize all the arrays to zero

overlap = 0.d0 
kinetic = 0.d0 
potentialA = 0.d0 
potentialB = 0.d0 
fock = 0.d0 
density = 0.d0 
hamiltonian = 0.d0 
unitary = 0.d0 
unitary_dagger = 0.d0
two_electron = 0.0 
temp = 0.d0


squared_rab = bond_length*bond_length

do p = 1, N
	do q = 1, N
		do i = 1, 2
			do j = 1, 2
                overlap(i,j) = overlap(i,j) + scaled_coeff(p,i)*scaled_coeff(q,j)*overlap_integral(scaled_expon(p,i), scaled_expon(q,j),abs(real(i-j))*squared_rab)
                kinetic(i,j) = kinetic(i,j) + scaled_coeff(p,i)*scaled_coeff(q,j)*kinetic_integral(scaled_expon(p,i), scaled_expon(q,j),abs(real(i-j))*squared_rab)
			enddo
		enddo
	enddo
enddo


call printing("Overlap",overlap)
call printing("kinetic",kinetic)

do p = 1, N
	do q = 1, N
        rap = scaled_expon(q,2)*bond_length/(scaled_expon(p,1)+scaled_expon(q,2))
        squared_rap = rap*rap
        squared_rbp = (bond_length - rap)**2

        potentialA(1,1) = potentialA(1,1) + scaled_coeff(p,1)*scaled_coeff(q,1)*nuclear_attraction_integral(scaled_expon(p,1), scaled_expon(q,1), 0.d0, 0.d0, Z_He)
        potentialA(1,2) = potentialA(1,2) + scaled_coeff(p,1)*scaled_coeff(q,2)*nuclear_attraction_integral(scaled_expon(p,1), scaled_expon(q,2), squared_rab, squared_rap, Z_He)
        potentialA(2,2) = potentialA(2,2) + scaled_coeff(p,2)*scaled_coeff(q,2)*nuclear_attraction_integral(scaled_expon(p,2), scaled_expon(q,2), 0.d0, squared_rab, Z_He)

        potentialB(1,1) = potentialB(1,1) + scaled_coeff(p,1)*scaled_coeff(q,1)*nuclear_attraction_integral(scaled_expon(p,1), scaled_expon(q,1), 0.d0, squared_rab, Z_H)
        potentialB(1,2) = potentialB(1,2) + scaled_coeff(p,1)*scaled_coeff(q,2)*nuclear_attraction_integral(scaled_expon(p,1), scaled_expon(q,2), squared_rab, squared_rbp, Z_H)
        potentialB(2,2) = potentialB(2,2) + scaled_coeff(p,2)*scaled_coeff(q,2)*nuclear_attraction_integral(scaled_expon(p,2), scaled_expon(q,2), 0.d0, 0.d0, Z_H)
	enddo
enddo

potentialA(2,1) = potentialA(1,2)
potentialB(2,1) = potentialB(1,2)


! Unitary transformation using the overlap matrix Gram Schmidt Orthogonalization
unitary(1,1) = 1.0d0/(dsqrt(2.d0*(1.d0+overlap(1,2))))
unitary(2,1) = unitary(1,1)
unitary(1,2) = 1.0d0/(dsqrt(2.d0*(1.d0-overlap(1,2))))
unitary(2,2) = -unitary(1,2)

rap = 0.d0 ; raq = 0.d0
rbp = 0.d0 ; rbq = 0.d0

! Evaluation of two-electron integrals
do i = 1, N
	do j = 1, N
		do k = 1, N
			do l = 1, N
                rap = scaled_expon(i,2)*bond_length/(scaled_expon(i,2)+scaled_expon(j,1))
                rbp = (bond_length - rap)
                raq = scaled_expon(k,2)*bond_length/(scaled_expon(k,2)+scaled_expon(l,1))
                rbq = (bond_length - raq)
                rpq = rap - raq 

                squared_rap = rap*rap
                squared_rbp = rbp*rbp
                squared_raq = raq*raq
                squared_rbq = rbq*rbq
                squared_rpq = rpq*rpq

                two_electron(1,1,1,1) = two_electron(1,1,1,1) + scaled_coeff(i,1)*scaled_coeff(j,1)*scaled_coeff(k,1)*scaled_coeff(l,1)*two_electron_integral(scaled_expon(i,1),scaled_expon(j,1),scaled_expon(k,1),scaled_expon(l,1),0.d0,0.d0,0.d0)
                two_electron(2,1,1,1) = two_electron(2,1,1,1) + scaled_coeff(i,2)*scaled_coeff(j,1)*scaled_coeff(k,1)*scaled_coeff(l,1)*two_electron_integral(scaled_expon(i,2),scaled_expon(j,1),scaled_expon(k,1),scaled_expon(l,1),squared_rab,0.d0,squared_rap)
                two_electron(2,2,1,1) = two_electron(2,2,1,1) + scaled_coeff(i,2)*scaled_coeff(j,2)*scaled_coeff(k,1)*scaled_coeff(l,1)*two_electron_integral(scaled_expon(i,2),scaled_expon(j,2),scaled_expon(k,1),scaled_expon(l,1),0.d0,0.d0,squared_rab)
                two_electron(2,1,2,1) = two_electron(2,1,2,1) + scaled_coeff(i,2)*scaled_coeff(j,1)*scaled_coeff(k,2)*scaled_coeff(l,1)*two_electron_integral(scaled_expon(i,2),scaled_expon(j,1),scaled_expon(k,2),scaled_expon(l,1),squared_rab,squared_rab,squared_rpq)
                two_electron(2,2,2,1) = two_electron(2,2,2,1) + scaled_coeff(i,2)*scaled_coeff(j,2)*scaled_coeff(k,2)*scaled_coeff(l,1)*two_electron_integral(scaled_expon(i,2),scaled_expon(j,2),scaled_expon(k,2),scaled_expon(l,1),0.d0,squared_rab,squared_rbq)
                two_electron(2,2,2,2) = two_electron(2,2,2,2) + scaled_coeff(i,2)*scaled_coeff(j,2)*scaled_coeff(k,2)*scaled_coeff(l,2)*two_electron_integral(scaled_expon(i,2),scaled_expon(j,2),scaled_expon(k,2),scaled_expon(l,2),0.d0,0.d0,0.d0)
			enddo
		enddo
	enddo
enddo

   two_electron(1,2,1,1)   = two_electron(2,1,1,1)
   two_electron(1,1,2,1)   = two_electron(2,1,1,1)
   two_electron(1,1,1,2)   = two_electron(2,1,1,1)
   two_electron(1,2,2,1)   = two_electron(2,1,2,1)
   two_electron(2,1,1,2)   = two_electron(2,1,2,1)
   two_electron(1,2,1,2)   = two_electron(2,1,2,1)
   two_electron(1,1,2,2)   = two_electron(2,2,1,1)
   two_electron(2,2,1,2)   = two_electron(2,2,2,1)
   two_electron(2,1,2,2)   = two_electron(2,2,2,1)
   two_electron(1,2,2,2)   = two_electron(2,2,2,1)


unitary_dagger = transpose(unitary)

! Building core hamiltonian
do i = 1, 2
	do j = 1, 2
        hamiltonian(i,j) = kinetic(i,j) + potentialA(i,j) + potentialB(i,j)
	enddo
enddo

call printing("potential due to A", potentialA)
call printing("potential due to B", potentialB)
call printing("unitary", unitary)
call printing("transpose of unitary ", unitary_dagger)
call printing("Core hamiltonian ", hamiltonian)


! SCF iterations begins
iter = 1
density = 0.0

do while (iter .le. maxiter)

! guess the density matrix (p in the textbook) first then build it from two electron integrals

    print*, "Iteration ", iter

	do i = 1, 2
		do j = 1, 2
            G(i,j) = 0.d0
			do k = 1, 2
				do l = 1, 2
                    G(i,j) = G(i,j) + density(k,l) * (two_electron(i,j,k,l) - 0.5d0 * two_electron(i,l,k,j))
				enddo
			enddo
		enddo
	enddo

call printing("Matrix G", G)

! Adding G to the core hamiltonian to form Fock matrix

fock = hamiltonian + G

electronic_energy = 0.d0
do i = 1, 2
	do j = 1 , 2
        electronic_energy = electronic_energy + 0.5d0 * density(i,j)*(fock(i,j)+hamiltonian(i,j))
	enddo
enddo

print*, "intermediate electronic_energy", iter, electronic_energy

call printing("Fock", fock)

! prepare fock dagger matrix by similarity transformation using the basis of unitary matrix

call dgemm("N","N",2,2,2,1.0d0,fock,2,unitary,2,0.d0,G,2)
call dgemm("N","N",2,2,2,1.0d0,unitary_dagger,2,G,2,0.d0,fock_dagger,2)

! diagonalize this fock dagger to get unitary matrix of coefficients and eigenvalues

call printing ("Fock dagger", fock_dagger)

call diagonalize(fock_dagger, eigenvalues, eigenvectors)


! coefficients in original basis (C) mulitply unitary matrix and eigenvectors
!C = matmul(unitary, eigenvectors)
call dgemm("N","N",2,2,2,1.0d0,unitary,2,eigenvectors,2,0.d0,C,2)



! Form new density matrix
do i = 1, 2
	do j = 1, 2
        old_density(i,j) = density(i,j)
        density(i,j) = 0.d0
        do k = 1 , 2/2
            density(i,j) = density(i,j) + 2.d0*C(i,k)*C(j,k)
        enddo
	enddo
enddo

call printing ("density", density)
call printing ("C prime / eigenvectors", eigenvectors)
call printing ("eigenvalues", eigenvalues)
call printing ("coefficients in the original basis", C)


delta=0.d0
do i = 1,2 
	do j = 1, 2
        delta = delta + (density(i,j)-old_density(i,j))**2
    enddo
enddo

delta = dsqrt(delta/4.d0)

print*, "change in density" ,delta

if ( delta .le. tolerance ) then
    print*, "......................................"
    print*, "Convergence achieved. exiting from scf"
    print*, "......................................"
    exit
else
    iter = iter + 1
    cycle
endif

 
end do  ! scf loop ends here

print*, " "

call printing ("Final Converged eigenvalues", eigenvalues)
call printing ("Final Converged coefficients in the original basis", C)
call printing ("Final Converged density", density)

! optional mulliken population
call dgemm("N","N",2,2,2,1.0d0,density,2,overlap,2,0.d0,mulliken,2)
call printing("mulliken population", mulliken)

total_energy = electronic_energy + Z_He*Z_H/bond_length

print*, "Electronic Energy : ", electronic_energy, "Ha"
print*, "Total Energy      : ", total_energy, "Ha"

call cpu_time(endtime)
write(*,'(a,2x,f12.3,2x,a)') "time required for the entire calculation : ", endtime - starttime, " seconds"
end program