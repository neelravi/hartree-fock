module general_data
save
integer, parameter  :: DP = kind(0.0d0)

! Global Constants
real(kind=DP) :: pi = 4.0d0*atan(1.0d0)

! system specific constants
integer, parameter       :: num_atoms = 2
real(kind=DP), parameter :: zeta_He = 2.0925
real(kind=DP), parameter :: zeta_H  = 1.24
real(kind=DP), parameter :: bond_length = 1.4632d0 ! in bohr units

real(kind=DP), parameter :: Z_He = 2
real(kind=DP), parameter :: Z_H  = 1


real(kind=DP), parameter :: tolerance = 1.d-4
integer, parameter :: maxiter = 10


end module general_data
