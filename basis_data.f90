module basis_data
use general_data
implicit double precision (a-h,o-z)
save
!integer, parameter  :: N = 3                  ! STO-3G basis

!real(kind=DP), dimension(3,3) :: coeff = reshape((/1.0, 0.0, 0.0, 0.678914, 0.430129, 0.0, 0.444634540, 0.535328140, 0.154328970/),(/3,3/))
!real(kind=DP), dimension(3,3) :: expon = reshape((/0.27095, 0.0, 0.0, 0.151623, 0.851819, 0.0, 0.109818, 0.405771, 2.22766/),(/3,3/))

!real(kind=DP), dimension(3,2) :: scaled_expon, scaled_coeff

!STO-6G from Basis Set Exchange (web)
integer, parameter  :: N = 6
real(kind=DP), dimension(N,2) :: scaled_coeff = reshape((/  &
        0.00916359628, 0.04936149294, 0.16853830490, 0.37056279970, 0.41649152980, 0.13033408410,  &    !STO-6G for He
        0.00916359628, 0.04936149294, 0.16853830490, 0.37056279970, 0.41649152980, 0.13033408410   &    !STO-6G for H
        /),(/N,2/))

real(kind=DP), dimension(N,2) :: scaled_expon = reshape((/   &
        65.98456824, 12.09819836, 3.384639924, 1.162715163, 0.451516322, 0.185959356, &                 !STO-6G for He
        35.52322122, 6.513143725, 1.822142904, 0.625955266, 0.243076747, 0.100112428  &                 !STO-6G for H
        /),(/N,2/))

integer, parameter :: nshell = 2
end module basis_data