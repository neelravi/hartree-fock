!Program to read the 6-311g basis set and print upon request                                                                                                    
!Written by Arun (arunkumar.cr@gmail.com) on 22/SEP/2016
!supervised by Ravindra

program read_6311g           
        implicit none

        integer::itrial, ntrial
        integer::iskip, nskip, iz, nz, ctr, iarg, true, narg, iorb
	real(kind(1d0))::ifactor
        real(kind(1d0)),allocatable,dimension(:)::icharge                
        real(kind(1d0)),allocatable,dimension(:)::ialpha, iconst, iconst1, iconst2
        character*15::ibegin, crdsystem, iorbname            
        character*10,allocatable,dimension(:)::iname
        integer,allocatable,dimension(:)::norb

        open(1, file="basis.in",   action="read")
        open(2, file="basis.out", action="write")
        read(1, *) crdsystem
        if(trim(crdsystem) .eq. 'spherical') then
                write(2, *) "Spherical"
                nskip = 17
                do iskip=1, nskip
                        read(1, *) 
                end do  !iskip

                nz   = 26         !total number of atoms
                ctr  = 0
                allocate(iname(nz), icharge(nz), norb(nz), stat=true)
                if(true .ne. 0) then
                        print*, "mem alloc err!"
                        stop
                end if  !true
                
                do iz=1, nz  
                        read(1, *) ibegin
                        if(trim(ibegin) .eq. '****') then
                                ctr = ctr + 1
                                read(1, *) iname(iz), icharge(iz), norb(iz)
                                write(2, *) "srno: ", iz, "element = ", trim(iname(iz))                     
                                write(2, *) "Printing basis info . . "
                           
                                do iorb=1, norb(iz) 

                                        read(1, *) iorbname, narg, ifactor
                                        write(2, *) trim(iorbname), narg, real(ifactor)

                                        allocate(ialpha(narg), iconst(narg), iconst1(narg), iconst2(narg), stat=true)

                                        if(true .ne. 0) then
                                                print*,"mem alloc err 2!"
                                                stop
                                        end if  !true

                                        do iarg=1, narg

                                                if(trim(iorbname) .eq. 'SP') then

                                                     read(1, *) ialpha(iarg), iconst1(iarg), iconst2(iarg)
                                                     iconst(iarg) = iconst1(iarg) + iconst2(iarg)
                                                     write(2, *) "          ", real(ialpha(iarg)), real(iconst1(iarg)), real(iconst2(iarg))

                                                else

                                                     read(1, *) ialpha(iarg), iconst(iarg)
                                                     write(2, *) "          ", real(ialpha(iarg)), real(iconst(iarg))

                                                end if  !iorbname

                                        end do !iarg
                                        deallocate(ialpha)
                                        deallocate(iconst)
                                        deallocate(iconst1)
                                        deallocate(iconst2) 
                                end do
                        end if  !ibegin
                end do          !iz
        end if  !spherical
        close(1)
        close(2)
end program read_6311g      
