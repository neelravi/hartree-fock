subroutine diagonalize(matrix, values, vectors)

double precision, intent(in)  :: matrix(2,2)
double precision, intent(out) :: vectors(2,2), values(2,2)
integer, parameter  :: m = 2     ! size of the matrix

integer, parameter  :: lda = m
integer, parameter  :: lwmax = 1000 

!     .. local scalars ..
integer   ::       info, lwork


double precision :: w( m ), work( lwmax )

values = 0.d0

lwork = -1
call dsyev( 'Vectors', 'Upper', m, matrix, lda, w, work, lwork, info )
lwork = min( lwmax, int( work( 1 ) ) )

!     solve eigenproblem.

call dsyev( 'Vectors', 'Upper', m, matrix, lda, w, work, lwork, info )

!     Check for convergence.
vectors = matrix
!      call printing("local eigen vectors", vectors)

values(1,1) = W(1) ; values(2,2) = W(2)

!      call printing("local eigen values", values)

if( info.gt.0 ) then
   write(*,*)'the algorithm failed to compute eigenvalues.'
   stop
end if

return
end subroutine
